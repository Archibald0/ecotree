# Test Tech EcoTree

## Requirments

- [PCOV](https://github.com/krakjoe/pcov/blob/develop/INSTALL.md)
- [XDebug](https://xdebug.org/docs/install) 

## Command

#### Launch exercise script
```shell
symfony console app:test-ecotree
```
> This command wil check non sold out articles by default.  However, with the `--all` option, it will perform the check on all articles, including those that are already sold out.


#### Fixtures
```shell
symfony console doctrine:fixture:load
```

#### Tests
```shell
vendor/bin/pest --coverage
```