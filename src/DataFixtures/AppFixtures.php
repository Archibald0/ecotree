<?php

namespace App\DataFixtures;

use App\Factory\ArticleFactory;
use App\Factory\OwnershipFactory;
use App\Factory\TreeFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $trees = TreeFactory::createMany(1000);
        $owners = OwnershipFactory::createMany(400);
        $articles = ArticleFactory::createMany(250);

        foreach ($trees as $tree) {
            $tree->disableAutoRefresh();
            if(rand(0,3)) {
                $tree->setOwnership($owners[rand(0, count($owners)-1)]->object());
            }
            $tree->setArticle($articles[rand(0, count($articles)-1)]->object());
            $tree->save();
        }

        $artSoldOut = ArticleFactory::createOne(['name' => 'Article Sold Out']);
        $treeSold = TreeFactory::createOne();
        $owner = OwnershipFactory::createOne();

        $treeSold->disableAutoRefresh();
        $treeSold->setOwnership($owner->object());
        $treeSold->setArticle($artSoldOut->object());
        $treeSold->save();
    }

}