<?php

namespace App\Entity;

use App\Repository\TreeRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TreeRepository::class)]
class Tree
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\ManyToOne(inversedBy: 'trees')]
    private ?Article $article = null;

    #[ORM\ManyToOne(inversedBy: 'tree')]
    private ?Ownership $ownership = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getArticle(): ?Article
    {
        return $this->article;
    }

    public function setArticle(?Article $article): static
    {
        $this->article = $article;

        return $this;
    }

    public function getOwnership(): ?Ownership
    {
        return $this->ownership;
    }

    public function setOwnership(?Ownership $ownership): static
    {
        $this->ownership = $ownership;

        return $this;
    }
}
