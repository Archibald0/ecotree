<?php

namespace App\Entity;

use App\Repository\ArticleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ArticleRepository::class)]
class Article
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'article', targetEntity: Tree::class)]
    private Collection $trees;

    #[ORM\Column]
    private bool $soldOut = false;

    public function __construct(
        string $name = null
    )
    {
        $this->setName($name);
        $this->trees = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Tree>
     */
    public function getTrees(): Collection
    {
        return $this->trees;
    }

    public function addTree(Tree $tree): static
    {
        if (!$this->trees->contains($tree)) {
            $this->trees->add($tree);
            $tree->setArticle($this);
        }

        return $this;
    }

    public function removeTree(Tree $tree): static
    {
        if ($this->trees->removeElement($tree)) {
            // set the owning side to null (unless already changed)
            if ($tree->getArticle() === $this) {
                $tree->setArticle(null);
            }
        }

        return $this;
    }

    public function isSoldOut(): ?bool
    {
        return $this->soldOut;
    }

    public function setSoldOut(bool $soldOut): static
    {
        $this->soldOut = $soldOut;

        return $this;
    }
}
