<?php

namespace App\Entity;

use App\Repository\OwnershipRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: OwnershipRepository::class)]
class Ownership
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'ownership', targetEntity: Tree::class)]
    private Collection $tree;

    public function __construct()
    {
        $this->tree = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Tree>
     */
    public function getTree(): Collection
    {
        return $this->tree;
    }

    public function addTree(Tree $tree): static
    {
        if (!$this->tree->contains($tree)) {
            $this->tree->add($tree);
            $tree->setOwnership($this);
        }

        return $this;
    }

    public function removeTree(Tree $tree): static
    {
        if ($this->tree->removeElement($tree)) {
            // set the owning side to null (unless already changed)
            if ($tree->getOwnership() === $this) {
                $tree->setOwnership(null);
            }
        }

        return $this;
    }
}
