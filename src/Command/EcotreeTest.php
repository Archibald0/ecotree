<?php

namespace App\Command;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class EcotreeTest extends Command
{
    public $articleRepository;

    /** @var EntityManager */
    public $em;

    public function __construct(ArticleRepository $articleRepository, EntityManagerInterface $entityManager)
    {
        parent::__construct('app:test-ecotree');

        $this->articleRepository = $articleRepository;
        $this->em = $entityManager;

        $this->addOption(
            'all', 'a',
            InputOption::VALUE_NEGATABLE,
            "Use it if you want to check the already sold out articles."
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Checking for Sold Out Articles');
        $checkAll = $input->getOption('all') === true;
        $stocks = $this->articleRepository->getStocks($checkAll);

        $io->progressStart(count($stocks));

        $soldOutArticleCount = 0;
        foreach ($stocks as $stock) {
            /** @var Article $article */
            $article = $stock['art'];
            if ($stock['stock_remaining'] > 0) {
                $article->setSoldOut(false);
            } else {
                $article->setSoldOut(true);
                $soldOutArticleCount++;
            }

            $io->progressAdvance();
        }
        $this->em->flush();

        $io->progressFinish();
        $io->newLine();
        $io->success(' Finish ! ' . $soldOutArticleCount . ' / ' . count($stocks) . ' articles are Sold Out.');

        return Command::SUCCESS;
    }

}