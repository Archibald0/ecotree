<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Article>
 *
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    public function save(Article $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Article $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getStocks(bool $checkAll = false): array
    {
        $query = $this->getEntityManager()->createQueryBuilder()
            ->select('article AS art, ( COUNT(trees) - COUNT(ownership)) AS stock_remaining')
            ->from(Article::class, 'article')
            ->leftJoin(
                'article.trees',
                'trees')
            ->leftJoin(
                'trees.ownership',
                'ownership');
        if(!$checkAll) {
            $query->where('article.soldOut = False');
        }
        $query->groupBy('article.id');

        return $query->getQuery()->getResult();
    }
}
