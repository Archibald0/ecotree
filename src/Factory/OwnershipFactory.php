<?php

namespace App\Factory;

use App\Entity\Ownership;
use App\Repository\OwnershipRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<Ownership>
 *
 * @method        Ownership|Proxy                     create(array|callable $attributes = [])
 * @method static Ownership|Proxy                     createOne(array $attributes = [])
 * @method static Ownership|Proxy                     find(object|array|mixed $criteria)
 * @method static Ownership|Proxy                     findOrCreate(array $attributes)
 * @method static Ownership|Proxy                     first(string $sortedField = 'id')
 * @method static Ownership|Proxy                     last(string $sortedField = 'id')
 * @method static Ownership|Proxy                     random(array $attributes = [])
 * @method static Ownership|Proxy                     randomOrCreate(array $attributes = [])
 * @method static OwnershipRepository|RepositoryProxy repository()
 * @method static Ownership[]|Proxy[]                 all()
 * @method static Ownership[]|Proxy[]                 createMany(int $number, array|callable $attributes = [])
 * @method static Ownership[]|Proxy[]                 createSequence(iterable|callable $sequence)
 * @method static Ownership[]|Proxy[]                 findBy(array $attributes)
 * @method static Ownership[]|Proxy[]                 randomRange(int $min, int $max, array $attributes = [])
 * @method static Ownership[]|Proxy[]                 randomSet(int $number, array $attributes = [])
 */
final class OwnershipFactory extends ModelFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     *
     * @todo inject services if required
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     *
     * @todo add your default values here
     */
    protected function getDefaults(): array
    {
        return [
            'name' => self::faker()->text(255),
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): self
    {
        return $this
            // ->afterInstantiate(function(Ownership $ownership): void {})
        ;
    }

    protected static function getClass(): string
    {
        return Ownership::class;
    }
}
