<?php

namespace App\Factory;

use App\Entity\Tree;
use App\Repository\TreeRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<Tree>
 *
 * @method        Tree|Proxy                     create(array|callable $attributes = [])
 * @method static Tree|Proxy                     createOne(array $attributes = [])
 * @method static Tree|Proxy                     find(object|array|mixed $criteria)
 * @method static Tree|Proxy                     findOrCreate(array $attributes)
 * @method static Tree|Proxy                     first(string $sortedField = 'id')
 * @method static Tree|Proxy                     last(string $sortedField = 'id')
 * @method static Tree|Proxy                     random(array $attributes = [])
 * @method static Tree|Proxy                     randomOrCreate(array $attributes = [])
 * @method static TreeRepository|RepositoryProxy repository()
 * @method static Tree[]|Proxy[]                 all()
 * @method static Tree[]|Proxy[]                 createMany(int $number, array|callable $attributes = [])
 * @method static Tree[]|Proxy[]                 createSequence(iterable|callable $sequence)
 * @method static Tree[]|Proxy[]                 findBy(array $attributes)
 * @method static Tree[]|Proxy[]                 randomRange(int $min, int $max, array $attributes = [])
 * @method static Tree[]|Proxy[]                 randomSet(int $number, array $attributes = [])
 */
final class TreeFactory extends ModelFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     *
     * @todo inject services if required
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     *
     * @todo add your default values here
     */
    protected function getDefaults(): array
    {
        return [
            'name' => self::faker()->text(255),
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): self
    {
        return $this
            // ->afterInstantiate(function(Tree $tree): void {})
        ;
    }

    protected static function getClass(): string
    {
        return Tree::class;
    }
}
