<?php

use App\Command\EcotreeTest;
use App\Entity\Article;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Tester\CommandTester;

dataset('stocks',
    [
        '3 stocks remaining' => [
            'art' => new Article(name: 'Forêt de Pins'),
            'stock_remaining' => 3,
            'expectedSoldOut' => false
        ],
        'No stock remaining' => [
            'art' => new Article(name: 'Forêt d\'Acacias'),
            'stock_remaining' => 0,
            'expectedSoldOut' => true
        ]
    ]
);

test('Return correct SoldOut', function (Article $article, int $stockRemaining, bool $expectedSoldOut) {
    $repoMock = Mockery::mock(ArticleRepository::class);
    $repoMock->shouldReceive('getStocks')->andReturn([
        [
            'art' => $article,
            'stock_remaining' => $stockRemaining
        ]
    ]);
    $entityManager = Mockery::mock(EntityManager::class);
    $entityManager->shouldReceive('flush');

    $commandTest = new CommandTester(new EcotreeTest($repoMock, $entityManager));
    $commandTest->execute([]);
    $commandTest->assertCommandIsSuccessful("Command ok");

    expect($article->isSoldOut())
        ->toBeBool()
        ->toBe($expectedSoldOut);

})->with('stocks');